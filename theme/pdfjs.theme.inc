<?php

function theme_pdfjs_files(&$vars) {
  // Reference configuration variables
  $items = &$vars['items'];
  $output = '';

  /**
   * Build an iframe referencing the matching display page or a colorbox link.
   */

  $settings = $vars['settings'];

  if (!empty($items)) {
    $counter = 0;
    $output .= '<div class="pdfjs-files-wrapper">';
    $library_source = libraries_detect('pdf.js');
    $library_path = $library_source['library path'];
    $viewer_source = base_path() . $library_path . '/web/viewer.html';

    foreach ($items as $item) {
      $url = file_create_url($item['file']['uri']);
      $viewer_url = $viewer_source . '?file=' . $url;

      $output .= '<div class="pdfjs-file">';

      if ($settings['colorbox']) {
        //Link to colorbox
        $output .= '<a class="pdfjs-colorbox-link" href="' . $viewer_url . '">' . $settings['colorbox_link_text'] . '</a>';
      }
      else {
        //Direct embed
        $output .= '<iframe src="' . $viewer_url . '" name="pdfjs-document-' . $counter . '"></iframe>';
      }

      if ($settings['download']) {
        $output .= '<a class="pdfjs-download-link" href="' . $url . '">' . $settings['download_link_text'] . '</a>';
      }

      $output .= '</div>';
      $counter++;
    }

    $output .= '</div>';

  }

  return $output;
}
