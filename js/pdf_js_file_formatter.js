(function($) {

  // Behavior to load FlexSlider
  Drupal.behaviors.pdfjsFileFormatterColorbox = {
    attach: function(context, settings) {
      $('.pdfjs-colorbox-link', context).once('pdfjs-colorbox').each(function() {
        $(this).colorbox({
          iframe: true,
          height: 750,
          width: 800,
        });
      });
    }
  };
}(jQuery));
